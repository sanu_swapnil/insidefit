
public class CommonObj {
	int i = 0;
	Boolean updated = false;
	
	public synchronized void put() throws Exception{
		if(updated) {
			wait();
		}
		i++;
		System.out.println(i);
		updated = true;
		notify();
	}
	
	public synchronized void get() throws Exception {
		if(!updated) {
			wait();
		}
		System.out.println(i);
		updated = false;
		notify();
	}
}
