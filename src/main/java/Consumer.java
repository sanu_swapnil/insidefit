
public class Consumer extends Thread {

	CommonObj obj;

	public Consumer(CommonObj obj) {
		this.obj = obj;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(200);
				obj.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
