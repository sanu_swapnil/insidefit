package com.dream.pojo;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class ProductResponse {

	private Integer id;

	private String name;

	private Long quantity;

	private String productDesc;

	private String imageUrl;

	private List<RateResponse> rates;

	private String related_img;

	private List<String> relatedImgList;
	
	public ProductResponse(Integer id, String name, Long quantity, String productDesc, String imageUrl,
			String related_img) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.productDesc = productDesc;
		this.imageUrl = imageUrl;
		this.related_img = related_img;
	}

	public ProductResponse() {
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the quantity
	 */
	public Long getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the productDesc
	 */
	public String getProductDesc() {
		return productDesc;
	}

	/**
	 * @param productDesc the productDesc to set
	 */
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the rates
	 */
	public List<RateResponse> getRates() {
		return rates;
	}

	/**
	 * @param rates the rates to set
	 */
	public void setRates(List<RateResponse> rates) {
		this.rates = rates;
	}

	/**
	 * @return the related_img
	 */
	public String getRelated_img() {
		return related_img;
	}

	/**
	 * @param related_img the related_img to set
	 */
	public void setRelated_img(String related_img) {
		this.related_img = related_img;
	}

	/**
	 * @return the relatedImgList
	 */
	public List<String> getRelatedImgList() {
		return relatedImgList;
	}

	/**
	 * @param relatedImgList the relatedImgList to set
	 */
	public void setRelatedImgList(List<String> relatedImgList) {
		this.relatedImgList = relatedImgList;
	}

	@Override
	public String toString() {
		return "ProductResponse [id=" + id + ", name=" + name + ", quantity=" + quantity + ", productDesc="
				+ productDesc + ", imageUrl=" + imageUrl + ", rates=" + rates + ", related_img=" + related_img
				+ ", relatedImgList=" + relatedImgList + "]";
	}

}
