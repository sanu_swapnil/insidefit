package com.dream.pojo;

import javax.validation.constraints.NotNull;

public class UpdateUserReq {
	
	@NotNull(message = "Username Field while password updation cannot be null")
	private String username;
	
	@NotNull(message = "Password Field while password updation cannot be null")
	private String password;

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	

	@Override
	public String toString() {
		return "UpdateUserReq [username=" + username + ", password=" + password + "]";
	}
	
}
