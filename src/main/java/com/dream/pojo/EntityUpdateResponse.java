package com.dream.pojo;

public class EntityUpdateResponse {
	
	private Boolean success;
	
	private String msg;

	public EntityUpdateResponse(Boolean success, String msg) {
		this.success = success;
		this.msg = msg;
	}
	

	/**
	 * @return the success
	 */
	public Boolean getSuccess() {
		return success;
	}


	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}


	@Override
	public String toString() {
		return "EntityUpdateResponse [success=" + success + ", msg=" + msg + "]";
	};
	
	
}
