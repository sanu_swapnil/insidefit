package com.dream.pojo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ContactSupport {

	
	@NotNull(message = "Name Field Cannot Be Null")
	@NotEmpty(message = "Name Field Cannot Be Empty")
    private String name;
	
	@NotNull(message = "Email Field Cannot Be Null")
	@NotEmpty(message = "Email Field Cannot Be Empty")
    private String email;
	
	@NotNull(message = "Description Field Cannot Be Null")
	@NotEmpty(message = "Description Field Cannot Be Empty")
    private String desc;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	@Override
	public String toString() {
		return "ContactSupport [name=" + name + ", email=" + email + ", desc=" + desc + "]";
	}
      
}
