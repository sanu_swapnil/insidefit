package com.dream.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:config.properties")
public class PropertiesFile {

	@Value("${alert.email}")
	private String alert_email;

	@Value("${origin}")
	private String origin;

	@Value("${forget.password.template}")
	private String otpSendingMessage;

	@Value("${order.confirmation.mail.template}")
	private String mailConfirmationTemplate;

	@Value("${contact.support.confirmation.template}")
	private String supportConfirmationTemplate;

	@Value("${self.email}")
	private String selfEmailId;

	public PropertiesFile() {
	}

	/**
	 * @return the alert_email
	 */
	public String getAlert_email() {
		return alert_email;
	}

	/**
	 * @param alert_email the alert_email to set
	 */
	public void setAlert_email(String alert_email) {
		this.alert_email = alert_email;
	}

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the otpSendingMessage
	 */
	public String getOtpSendingMessage() {
		return otpSendingMessage;
	}

	/**
	 * @param otpSendingMessage the otpSendingMessage to set
	 */
	public void setOtpSendingMessage(String otpSendingMessage) {
		this.otpSendingMessage = otpSendingMessage;
	}

	/**
	 * @return the mailConfirmationTemplate
	 */
	public String getMailConfirmationTemplate() {
		return mailConfirmationTemplate;
	}

	/**
	 * @param mailConfirmationTemplate the mailConfirmationTemplate to set
	 */
	public void setMailConfirmationTemplate(String mailConfirmationTemplate) {
		this.mailConfirmationTemplate = mailConfirmationTemplate;
	}

	/**
	 * @return the supportConfirmationTemplate
	 */
	public String getSupportConfirmationTemplate() {
		return supportConfirmationTemplate;
	}

	/**
	 * @param supportConfirmationTemplate the supportConfirmationTemplate to set
	 */
	public void setSupportConfirmationTemplate(String supportConfirmationTemplate) {
		this.supportConfirmationTemplate = supportConfirmationTemplate;
	}

	/**
	 * @return the selfEmailId
	 */
	public String getSelfEmailId() {
		return selfEmailId;
	}

	/**
	 * @param selfEmailId the selfEmailId to set
	 */
	public void setSelfEmailId(String selfEmailId) {
		this.selfEmailId = selfEmailId;
	}

	@Override
	public String toString() {
		return "PropertiesFile [alert_email=" + alert_email + ", origin=" + origin + ", otpSendingMessage="
				+ otpSendingMessage + ", mailConfirmationTemplate=" + mailConfirmationTemplate
				+ ", supportConfirmationTemplate=" + supportConfirmationTemplate + ", selfEmailId=" + selfEmailId + "]";
	}

}
