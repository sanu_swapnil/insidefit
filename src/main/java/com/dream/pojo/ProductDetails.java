package com.dream.pojo;

import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class ProductDetails {

	private Integer id;
	
	private String productName;
	
	private String currency;
	
	private Float price;

	private Integer rateCount;

	private List<VideoDetails> videos;

	private String productImages;

	private String productTitle;

	private String productHeading;

	private String description;

	private List<ReviewDetails> reviews;

	private String productSub;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the rateCount
	 */
	public Integer getRateCount() {
		return rateCount;
	}

	/**
	 * @param rateCount the rateCount to set
	 */
	public void setRateCount(Integer rateCount) {
		this.rateCount = rateCount;
	}

	/**
	 * @return the videos
	 */
	public List<VideoDetails> getVideos() {
		return videos;
	}

	/**
	 * @param videos the videos to set
	 */
	public void setVideos(List<VideoDetails> videos) {
		this.videos = videos;
	}

	/**
	 * @return the productImages
	 */
	public String getProductImages() {
		return productImages;
	}

	/**
	 * @param productImages the productImages to set
	 */
	public void setProductImages(String productImages) {
		this.productImages = productImages;
	}

	/**
	 * @return the productTitle
	 */
	public String getProductTitle() {
		return productTitle;
	}

	/**
	 * @param productTitle the productTitle to set
	 */
	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	/**
	 * @return the productHeading
	 */
	public String getProductHeading() {
		return productHeading;
	}

	/**
	 * @param productHeading the productHeading to set
	 */
	public void setProductHeading(String productHeading) {
		this.productHeading = productHeading;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the reviews
	 */
	public List<ReviewDetails> getReviews() {
		return reviews;
	}

	/**
	 * @param reviews the reviews to set
	 */
	public void setReviews(List<ReviewDetails> reviews) {
		this.reviews = reviews;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the price
	 */
	public Float getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Float price) {
		this.price = price;
	}

	/**
	 * @return the productSub
	 */
	public String getProductSub() {
		return productSub;
	}

	/**
	 * @param productSub the productSub to set
	 */
	public void setProductSub(String productSub) {
		this.productSub = productSub;
	}

	@Override
	public String toString() {
		return "ProductDetails [id=" + id + ", productName=" + productName + ", currency=" + currency + ", price="
				+ price + ", rateCount=" + rateCount + ", videos=" + videos + ", productImages=" + productImages
				+ ", productTitle=" + productTitle + ", productHeading=" + productHeading + ", description="
				+ description + ", reviews=" + reviews + ", productSub=" + productSub + "]";
	}

}
