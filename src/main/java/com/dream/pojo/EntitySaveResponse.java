package com.dream.pojo;

public class EntitySaveResponse {

	private Boolean success;
	
	private Integer id;
	
	public EntitySaveResponse(Boolean success, Integer id) {
		this.success = success;
		this.id = id;
	}

	/**
	 * @return the success
	 */
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

}
