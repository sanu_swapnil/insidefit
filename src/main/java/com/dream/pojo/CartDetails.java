package com.dream.pojo;

public class CartDetails {

	private Integer id;
	
	private Integer productId;

	private String price;

	private String category;

	private String size;

	private Integer quantity;

	private Integer userId;

	private String productName;
	
	private String productSub;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the productSub
	 */
	public String getProductSub() {
		return productSub;
	}

	/**
	 * @param productSub the productSub to set
	 */
	public void setProductSub(String productSub) {
		this.productSub = productSub;
	}

	@Override
	public String toString() {
		return "CartDetails [id=" + id + ", productId=" + productId + ", price=" + price + ", category=" + category
				+ ", size=" + size + ", quantity=" + quantity + ", userId=" + userId + ", productName=" + productName
				+ ", productSub=" + productSub + "]";
	}

}
