package com.dream.pojo;

import java.util.HashMap;
import java.util.Map;

public class OtpMapper {

	private Map<String, OtpHolder> otpMapper = new HashMap<String, OtpHolder>();

	private OtpMapper() {
	}

	private static OtpMapper instance;

	public static OtpMapper getInstance() {
		if (instance == null) {
			synchronized (OtpMapper.class) {
				if (instance == null) {
					instance = new OtpMapper();
				}
			}
		}
		return instance;
	}

	/**
	 * @return the otpMapper
	 */
	public Map<String, OtpHolder> getOtpMapper() {
		return otpMapper;
	}

	/**
	 * @param otpMapper the otpMapper to set
	 */
	public void setOtpMapper(Map<String, OtpHolder> otpMapper) {
		this.otpMapper = otpMapper;
	}

	@Override
	public String toString() {
		return "OtpMapper [otpMapper=" + otpMapper + "]";
	}

	
}
