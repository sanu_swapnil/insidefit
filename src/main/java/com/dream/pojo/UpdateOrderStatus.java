package com.dream.pojo;


import javax.validation.constraints.NotNull;

public class UpdateOrderStatus {

	@NotNull(message = "Field Id Cannot be Null or Empty")
	private Integer id;
	
	@NotNull(message = "Field Name Cannot be Null")
	private Status status;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "UpdateOrderStatus [id=" + id + ", status=" + status + "]";
	}
	
}
