package com.dream.pojo;

import javax.validation.constraints.NotNull;

public class DeleteCartProduct {

	@NotNull(message = "Given Field Id cannot be Null")
	private Integer id;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DeleteCartProduct [id=" + id + "]";
	}
	
	
	
	
}
