package com.dream.pojo;

import java.util.Calendar;

public class OtpHolder {

	private String otp;
	
	private Long startTime;
	
	private Long expireTime;
	
	public OtpHolder(String otp){
		this.otp = otp;
		long time = Calendar.getInstance().getTimeInMillis();
		this.startTime = time;
		this.expireTime = time + 360000;
	}

	/**
	 * @return the otp
	 */
	public String getOtp() {
		return otp;
	}

	/**
	 * @return the startTime
	 */
	public Long getStartTime() {
		return startTime;
	}

	/**
	 * @return the expireTime
	 */
	public Long getExpireTime() {
		return expireTime;
	}

	@Override
	public String toString() {
		return "OtpHolder [otp=" + otp + ", startTime=" + startTime + ", expireTime=" + expireTime + "]";
	}
	
}
