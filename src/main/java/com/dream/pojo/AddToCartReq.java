package com.dream.pojo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AddToCartReq {

	@NotNull(message="PRODUCT_ID cannot be missing or empty")
	private Integer productId;

	@NotNull(message="PRICE cannot be missing or empty")
	private String price;

	@NotNull(message="CATEGORY cannot be missing or empty")
	private String category;

	@NotNull(message="SIZE cannot be missing or empty")
	private String size;

	@NotNull(message="QUANTITY cannot be missing or empty")
	private Integer quantity;

	@NotNull(message="USER_ID cannot be missing or empty")
	private Integer userId;

	@NotNull(message="Product Name cannot be Missing")
	@NotBlank(message = "Product Name cannot be Empty")
	private String productName;
	
	@NotNull(message="Product Subtitle cannot missing")
	@NotBlank(message = "Product Subtitle cannot empty")
	private String productSub;
	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the productSub
	 */
	public String getProductSub() {
		return productSub;
	}

	/**
	 * @param productSub the productSub to set
	 */
	public void setProductSub(String productSub) {
		this.productSub = productSub;
	}

	@Override
	public String toString() {
		return "AddToCartReq [productId=" + productId + ", price=" + price + ", category=" + category + ", size=" + size
				+ ", quantity=" + quantity + ", userId=" + userId + ", productName=" + productName + ", productSub="
				+ productSub + "]";
	}

}
