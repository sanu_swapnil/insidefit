package com.dream.pojo;

public class RateResponse {
	
	private Integer id;

	private String dimension;

	private String currency;

	private Float actualPrice;

	private Float afterDiscountPrice;

	private Float discountInPercentage;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the dimension
	 */
	public String getDimension() {
		return dimension;
	}

	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the actualPrice
	 */
	public Float getActualPrice() {
		return actualPrice;
	}

	/**
	 * @param actualPrice the actualPrice to set
	 */
	public void setActualPrice(Float actualPrice) {
		this.actualPrice = actualPrice;
	}

	/**
	 * @return the afterDiscountPrice
	 */
	public Float getAfterDiscountPrice() {
		return afterDiscountPrice;
	}

	/**
	 * @param afterDiscountPrice the afterDiscountPrice to set
	 */
	public void setAfterDiscountPrice(Float afterDiscountPrice) {
		this.afterDiscountPrice = afterDiscountPrice;
	}

	/**
	 * @return the discountInPercentage
	 */
	public Float getDiscountInPercentage() {
		return discountInPercentage;
	}

	/**
	 * @param discountInPercentage the discountInPercentage to set
	 */
	public void setDiscountInPercentage(Float discountInPercentage) {
		this.discountInPercentage = discountInPercentage;
	}

	@Override
	public String toString() {
		return "RateResponse [id=" + id + ", dimension=" + dimension + ", currency=" + currency + ", actualPrice="
				+ actualPrice + ", afterDiscountPrice=" + afterDiscountPrice + ", discountInPercentage="
				+ discountInPercentage + "]";
	}

	
}
