package com.dream.pojo;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class OrderRequestBean {

	@NotNull(message="Delivery Address cannot be missing")
	@NotEmpty(message = "Delivery Address cannot be Empty")
	private String deliveryAddress;

	@NotNull(message="UserId cannot be missing or empty")
	private Integer userId;
	
	@NotNull(message="UserName cannot be missing")
	@NotEmpty(message = "UserName cannot be Empty")
	private String username;

//	@NotNull(message="ProductIds cannot be Missing")
//	@NotEmpty(message = "ProductIds cannot be Empty")
	private List<Integer> productIds;

	@NotNull(message="Order Amount cannot be missing or empty")
	private Float orderAmount;
	
	@NotNull(message="PaymentId cannot be missing")
	@NotEmpty(message = "PaymentId cannot be Empty")
	private String paymentId;
	
	@NotNull(message="PaymentId cannot be missing")
	@NotEmpty(message = "PaymentId cannot be Empty")
	
	private String productDetails;
	
	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	

	/**
	 * @return the orderAmount
	 */
	public Float getOrderAmount() {
		return orderAmount;
	}

	/**
	 * @param orderAmount the orderAmount to set
	 */
	public void setOrderAmount(Float orderAmount) {
		this.orderAmount = orderAmount;
	}


	
	/**
	 * @return the paymentId
	 */
	public String getPaymentId() {
		return paymentId;
	}

	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * @return the productIds
	 */
	public List<Integer> getProductIds() {
		return productIds;
	}

	/**
	 * @param productIds the productIds to set
	 */
	public void setProductIds(List<Integer> productIds) {
		this.productIds = productIds;
	}

	/**
	 * @return the productDetails
	 */
	public String getProductDetails() {
		return productDetails;
	}

	/**
	 * @param productDetails the productDetails to set
	 */
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "OrderRequestBean [deliveryAddress=" + deliveryAddress + ", userId=" + userId + ", username=" + username
				+ ", productIds=" + productIds + ", orderAmount=" + orderAmount + ", paymentId=" + paymentId
				+ ", productDetails=" + productDetails + "]";
	}

	
}
