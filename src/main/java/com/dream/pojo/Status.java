package com.dream.pojo;

public enum Status {

	
	COMPLETED("COMPLETED"),
	ORDERED("ORDERED"),
	INPROGRESS("INPROGRESS"),
	SHIPPED("SHIPPED"),
	REJECTED("REJECTED");
	
	private Status(String name) {
		this.name = name;
	}

	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	
}
