package com.dream.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dream.entity.CartEntity;

@Repository
public interface CartRepository extends JpaRepository<CartEntity, Integer> {

	public List<CartEntity> findByUserId(Integer userId);

	public List<CartEntity> findByUserIdAndProductId(Integer userId, Integer prodId);

	public void deleteById(Integer cartId);
}
