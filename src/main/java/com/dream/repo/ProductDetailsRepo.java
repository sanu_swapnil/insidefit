package com.dream.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dream.entity.ProductDetailsEntity;

@Repository
public interface ProductDetailsRepo extends JpaRepository<ProductDetailsEntity, Integer> {

	public List<ProductDetailsEntity> findAll();
}
