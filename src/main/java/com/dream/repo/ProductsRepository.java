package com.dream.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dream.entity.Products;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Integer>{
	
	public List<Products> findAll();
		
}
