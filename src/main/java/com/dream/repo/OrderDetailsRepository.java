package com.dream.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dream.entity.OrderDetailsEntity;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetailsEntity, Integer> {

	@Query("FROM OrderDetailsEntity order_details WHERE order_details.userId = :userId order by id desc ")
	public List<OrderDetailsEntity> findOrderByUserId(@Param("userId") Integer id);

//	@Query("SELECT order FROM order_details order WHERE order.userId=(SELECT userId FROM order_details o o.startTime <= :endTime and o.startTime >= :startTime)")
//	public List<OrderDetailsEntity> findOrderByRangeAndUser(@Param("userId") Integer id,
//			@Param("startTime") Long startTime, @Param("endTime") Long endTime);

	@Query("FROM OrderDetailsEntity order WHERE order.startTime >= :startTime and order.startTime <= :endTime")
	public List<OrderDetailsEntity> findOrderByRange(@Param("startTime") Long startTime,
			@Param("endTime") Long endTime);

}
