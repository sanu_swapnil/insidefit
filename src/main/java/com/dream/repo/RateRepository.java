/**
 * 
 */
package com.dream.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dream.entity.Rate;

/**
 * @author swapnil
 *
 */
@Repository
public interface RateRepository extends JpaRepository<Rate, Integer> {

}
