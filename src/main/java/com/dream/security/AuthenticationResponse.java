package com.dream.security;

import com.dream.pojo.UserDetailsResponse;

public class AuthenticationResponse {

	private String jwt;
	
	private UserDetailsResponse userDetailsResponse;
	
	public AuthenticationResponse(String jwt, UserDetailsResponse userDetailsResponse) {
		super();
		this.jwt = jwt;
		this.userDetailsResponse = userDetailsResponse;
	}

	/**
	 * @return the jwt
	 */
	public String getJwt() {
		return jwt;
	}

	/**
	 * @return the userDetailsResponse
	 */
	public UserDetailsResponse getUserDetailsResponse() {
		return userDetailsResponse;
	}
	
}
