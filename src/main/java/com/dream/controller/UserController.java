package com.dream.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.PropertiesFile;
import com.dream.pojo.UpdateUserReq;
import com.dream.security.AuthenticationRequest;
import com.dream.security.AuthenticationResponse;
import com.dream.security.JwtUtil;
import com.dream.security.UserPrincipals;
import com.dream.service.SaveUserReq;
import com.dream.service.UserDetailsServiceImpl;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private PropertiesFile prop;
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtUtil jwtutil;

	@Autowired
	private Logger logger = LogManager.getLogger();

	@RequestMapping("/")
	public String home() {
		System.out.println(prop.getAlert_email());
		System.out.println(prop.getOrigin());
		return "home.jsp";
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> authenticateUser(@RequestBody AuthenticationRequest request) throws Exception {
		logger.info("Authenticating user with username: " + request.getUsername());
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(),
					request.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect username and password : ", e);
		}
		final UserPrincipals userDetails = userDetailsService.loadUserByUsername(request.getUsername());
		final String jwt = jwtutil.generateToken(userDetails);
		logger.info("Authentication Success with username: " + request.getUsername());
		AuthenticationResponse authResp = new AuthenticationResponse(jwt, userDetails.getUserDetails());
		return ResponseEntity.ok(authResp);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<?> save(@Valid @RequestBody SaveUserReq saveUserReq) {
		logger.info("Save User: " + saveUserReq.toString());
		Boolean isUserPresent = userDetailsService.userExist(saveUserReq.getEmail());
		if(isUserPresent)
			return new ResponseEntity<>("Entered Email Exists",HttpStatus.CONFLICT);
		EntitySaveResponse saveResponse = userDetailsService.saveUser(saveUserReq);
		logger.info("Save User Response: " + saveResponse.toString());
		return ResponseEntity.ok(saveResponse);
	}

	@RequestMapping(value = "/forgetPassword", method = RequestMethod.GET)
	public ResponseEntity<?> forgetPassword(@RequestParam(required = true) String username) {
		logger.info("Forget Password Called for User " + username);
		Boolean forgetEmailResponse = userDetailsService.forgetPassword(username);
		logger.info("Forget Msg Response: " + forgetEmailResponse);
		return ResponseEntity.ok(forgetEmailResponse);
	}

	@RequestMapping(value = "/validateOtp", method = RequestMethod.GET)
	public ResponseEntity<?> validateOtp(@RequestParam(required = true) String username,
			@RequestParam(required = true) String otp) {
		logger.info("Validating Otp for user " + username);
		Boolean forgetEmailResponse = userDetailsService.validateOtp(username, otp);
		logger.info("Otp Validation Response for user with id:  " + username + " is " + forgetEmailResponse);
		return ResponseEntity.ok(forgetEmailResponse);
	}

	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public ResponseEntity<?> forgetPassword(@Valid @RequestBody UpdateUserReq updateUserReq) {
		logger.info("Forget Password Called for User " + updateUserReq.toString());
		Boolean updatePasswordStatus = userDetailsService.updatePassword(updateUserReq.getUsername(),
				updateUserReq.getPassword());
		logger.info("Update Password Status: " + updatePasswordStatus);
		return ResponseEntity.ok(updatePasswordStatus);
	}

}
