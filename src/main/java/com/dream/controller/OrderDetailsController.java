package com.dream.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.EntityUpdateResponse;
import com.dream.pojo.OrderRequestBean;
import com.dream.pojo.UpdateOrderStatus;
import com.dream.pojo.UserOrderDetailsResponse;
import com.dream.service.OrderDetailsService;

@RestController
@RequestMapping(value = "/order")
@CrossOrigin(origins = "*")
public class OrderDetailsController {

	@Autowired
	private Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private OrderDetailsService orderDetailsService;

	@RequestMapping(value = "/saveOrder", method = RequestMethod.POST)
	public ResponseEntity<?> save(@Valid @RequestBody OrderRequestBean order) {
		logger.info("Save Order: " + order.toString());
		EntitySaveResponse saveResponse = orderDetailsService.saveOrder(order);
		logger.info("Save Order Response: " + saveResponse.toString());
		return ResponseEntity.ok(saveResponse);
	}

	@RequestMapping(value = "/getOrderDetails", method = RequestMethod.GET)
	public ResponseEntity<?> getAllOrderDetails() {
		logger.info("Get All Order Details");
		List<UserOrderDetailsResponse> orderDetails = orderDetailsService.getAllOrderDetails();
		logger.info("Get All Order Details End: " + orderDetails.toString());
		return ResponseEntity.ok(orderDetails);
	}

	@RequestMapping(value = "/getOrderDetailsByUser", method = RequestMethod.GET)
	public ResponseEntity<?> getOrderDetailsByUserId(@RequestParam(required = true) Integer id) {
		logger.info("Get Order For UserId : " + id);
		List<UserOrderDetailsResponse> orderDetails = orderDetailsService.getOrderDetailsByUserId(id);
		logger.info("Get Order For UserId : " + id + " : " + orderDetails.toString());
		return ResponseEntity.ok(orderDetails);
	}

	@RequestMapping(value = "/getOrderDetailsByRange", method = RequestMethod.GET)
	public ResponseEntity<?> getOrderDetailsByRange(@RequestParam(required = true) Long startTime,
			@RequestParam(required = true) Long endTime) {
		logger.info("Get Order For start_time: " + startTime + "end_time: " + endTime);
		List<UserOrderDetailsResponse> orderDetails = orderDetailsService.getOrdersByRange(startTime, endTime);
		logger.info("Get Order response :" + orderDetails.toString());
		return ResponseEntity.ok(orderDetails);
	}

	@RequestMapping(value = "/getOrderDetailsByUserAndRange", method = RequestMethod.GET)
	public ResponseEntity<?> getOrderDetailsByUserAndRange(@RequestParam(required = true) Integer id,
			@RequestParam(required = true) Long startTime, @RequestParam(required = true) Long endTime) {
		logger.info("Get Order For UserId : " + id + "start_time: " + startTime + "end_time: " + endTime);
		List<UserOrderDetailsResponse> orderDetails = orderDetailsService.getOrdersByRangeAndUserId(startTime, endTime,
				id);
		logger.info("Get Order response :" + orderDetails.toString());
		return ResponseEntity.ok(orderDetails);
	}

	@RequestMapping(value = "/updateOrder", method = RequestMethod.POST)
	public ResponseEntity<?> getOrderDetailsByUserAndRange(@Valid @RequestBody UpdateOrderStatus updateOrderStatus) {

		logger.info("Updating Order For Request: " + updateOrderStatus.toString());
		EntityUpdateResponse orderUpdationStatus = orderDetailsService.updateOrderStatus(updateOrderStatus.getId(),
				updateOrderStatus.getStatus().getName());
		logger.info("Get Order response :" + orderUpdationStatus.toString());
		return ResponseEntity.ok(orderUpdationStatus);
	}

}
