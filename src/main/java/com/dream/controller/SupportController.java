package com.dream.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dream.pojo.ContactSupport;
import com.dream.service.ContactSupportService;

@RestController
@RequestMapping(value = "/contact")
@CrossOrigin(origins = "*")
public class SupportController {

	@Autowired
	private Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private ContactSupportService contactSupportService;

	@RequestMapping(method = RequestMethod.POST, value = "/contactSupport")
	public ResponseEntity<?> contactSupport(@Valid @RequestBody ContactSupport contact) {
		contactSupportService.contactSupport(contact);
		return ResponseEntity.ok(true);
	}
}
