package com.dream.controller;

import java.util.List;
import java.util.Timer;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dream.pojo.AddToCartReq;
import com.dream.pojo.CartDetails;
import com.dream.pojo.DeleteCartProduct;
import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.EntityUpdateResponse;
import com.dream.service.CartService;
import com.dream.service.OtpMapTimer;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/cart")
public class CartController {

	private static Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private CartService cartService;

	@PostConstruct
	public void doTask() {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new OtpMapTimer(), 5000, 43200000);
	}

	public void hello() {

	}

	@RequestMapping(value = "/addToCart", method = RequestMethod.POST)
	public ResponseEntity<?> addToCart(@Valid @RequestBody AddToCartReq addToCart) {
		logger.info("Add To Cart: " + addToCart.toString());
		EntitySaveResponse saveResponse = cartService.addToCart(addToCart);
		logger.info("Add To Cart Response: " + saveResponse.toString());
		return ResponseEntity.ok(saveResponse);
	}

	@RequestMapping(value = "/cartDetails", method = RequestMethod.GET)
	public ResponseEntity<?> getCartDetails(@RequestParam(required = true) Integer userId) {
		logger.info("Get CartDetails called for UserId: " + userId);
		List<CartDetails> cartDetailsList = cartService.getCartDetailsByUser(userId);
		logger.info("Cart Details List Response " + cartDetailsList.toString());
		return ResponseEntity.ok(cartDetailsList);
	}

	@RequestMapping(value = "/removeFromCart", method = RequestMethod.POST)
	public ResponseEntity<?> removeFromCart(@Valid @RequestBody DeleteCartProduct removeFromCart) {
		logger.info("Remove From Cart: " + removeFromCart.toString());
		EntityUpdateResponse updateResponse = cartService.removeFromCart(removeFromCart.getId());
		logger.info("Remove From Cart Response: " + updateResponse.toString());
		return ResponseEntity.ok(updateResponse);
	}

}
