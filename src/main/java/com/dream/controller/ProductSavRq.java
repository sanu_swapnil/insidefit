/**
 * 
 */
package com.dream.controller;

import java.util.List;

/**
 * @author swapnil
 *
 */
public class ProductSavRq {

	private Integer id;

	private String name;

	private Long startTimeInMillis;

	private Long endTimeInMillis;

	private Long quantity;

	private String productDesc;

	private String imageUrl;
	
	private List<RateDetails> rates;
	
	private String related_img;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the startTimeInMillis
	 */
	public Long getStartTimeInMillis() {
		return startTimeInMillis;
	}

	/**
	 * @param startTimeInMillis the startTimeInMillis to set
	 */
	public void setStartTimeInMillis(Long startTimeInMillis) {
		this.startTimeInMillis = startTimeInMillis;
	}

	/**
	 * @return the endTimeInMillis
	 */
	public Long getEndTimeInMillis() {
		return endTimeInMillis;
	}

	/**
	 * @param endTimeInMillis the endTimeInMillis to set
	 */
	public void setEndTimeInMillis(Long endTimeInMillis) {
		this.endTimeInMillis = endTimeInMillis;
	}

	/**
	 * @return the quantity
	 */
	public Long getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the productDesc
	 */
	public String getProductDesc() {
		return productDesc;
	}

	/**
	 * @param productDesc the productDesc to set
	 */
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the rates
	 */
	public List<RateDetails> getRates() {
		return rates;
	}

	/**
	 * @param rates the rates to set
	 */
	public void setRates(List<RateDetails> rates) {
		this.rates = rates;
	}
	
	/**
	 * @return the related_img
	 */
	public String getRelated_img() {
		return related_img;
	}

	/**
	 * @param related_img the related_img to set
	 */
	public void setRelated_img(String related_img) {
		this.related_img = related_img;
	}

	@Override
	public String toString() {
		return "ProductSavRq [id=" + id + ", name=" + name + ", startTimeInMillis=" + startTimeInMillis
				+ ", endTimeInMillis=" + endTimeInMillis + ", quantity=" + quantity + ", productDesc=" + productDesc
				+ ", imageUrl=" + imageUrl + ", rates=" + rates + ", related_img=" + related_img + "]";
	}
}
