package com.dream.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.ProductDetails;
import com.dream.pojo.ProductResponse;
import com.dream.repo.ProductsRepository;
import com.dream.service.ProductService;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "*")
public class ProductController {

	@Autowired
	private ProductsRepository productsRepository;

//	@PostConstruct
//	void dothis(){
//		List<Products> products = 
//				productsRepository.findAll();
//		System.out.println("Products: " + products.toString() + "Rates: "+products.get(0).getRates()); 
//	}

	@Autowired
	private Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(HttpServletRequest request) {
		logger.info("Get all products called");
		List<ProductResponse> prodRes = productService.getAllProducts(request.getContextPath());
		return ResponseEntity.ok(prodRes);
	}

	@RequestMapping(value = "/getAllProducts", method = RequestMethod.GET)
	public ResponseEntity<?> getAllProducts(HttpServletRequest request) {
		logger.info("Get all products called");
		List<ProductDetails> prodRes = productService.getAllProductDetails();
		return ResponseEntity.ok(prodRes);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody ProductSavRq product) {
		logger.info("Save Product: " + product.toString());
		EntitySaveResponse saveResponse = productService.saveProductResponse(product);
		logger.info("Save Product Response: " + saveResponse.toString());
		return ResponseEntity.ok(saveResponse);
	}

}
