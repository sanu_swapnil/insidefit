package com.dream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@SpringBootApplication
@EnableAsync
public class DreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(DreamApplication.class, args);
	}

	@Bean
	public Logger getLogger() {
		return LogManager.getLogger();
	}

	@Bean
	public MapperFactory getMapperFactory() {
		return new DefaultMapperFactory.Builder().build();
	}

//	@Bean
//	public OtpMapper otpMapper() {
//		return new OtpMapper(new HashMap<>());
//	}

//	@Bean
//	public PropertiesFile getProperties() {
//		PropertiesFile p = new PropertiesFile();
////		System.out.println("Property File: " + p.toString());
//		return p;
//	}
}
