package com.dream.service;

import com.dream.pojo.EntitySaveResponse;

public interface CustomUserDetailsService {
	
	public EntitySaveResponse saveUser(SaveUserReq saveUserReq);

	public Boolean forgetPassword(String username);
	
	public Boolean validateOtp(String username , String otp);
	
	public Boolean updatePassword(String username , String password);
	
	public Boolean userExist(String username);
}
