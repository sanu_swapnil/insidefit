package com.dream.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;

@Service
public class MapperService {

	private static Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private MapperFactory mapperFactory;

	/**
	 * Mapping Utils
	 * 
	 * @param src
	 * @param destination
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T mapResponse(T src, T destination) {
		logger.debug("Mapping Products Src: " + src.toString());
		if (src == null || destination == null)
			throw new NullPointerException(
					"Both the params must not be Null .. [src]=[" + src + "] ,Destination: " + destination);
		mapperFactory.classMap(src.getClass(), destination.getClass());
		MapperFacade mapper = mapperFactory.getMapperFacade();
		destination = (T) mapper.map(src, destination.getClass());
		logger.debug("Mapping Products Destination: " + destination.toString());
		return destination;
	}
}
