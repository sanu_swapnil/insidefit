package com.dream.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dream.controller.ProductSavRq;
import com.dream.entity.ProductDetailsEntity;
import com.dream.entity.Products;
import com.dream.entity.Rate;
import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.ProductDetails;
import com.dream.pojo.ProductResponse;
import com.dream.repo.ProductDetailsRepo;
import com.dream.repo.ProductsRepository;
import com.dream.repo.RateRepository;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;

@Service
public class ProductServiceImpl implements ProductService {

	private static Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private ProductsRepository productsRepository;

	@Autowired
	private RateRepository rateRepository;

	@Autowired
	private MapperFactory mapperFactory;
	
	@Autowired
	private ProductDetailsRepo productsDetailsRepository;

	@Override
	public List<ProductResponse> getAllProducts(String context) {
		logger.info("Getting all products service start");
		List<Products> products = productsRepository.findAll();
		List<ProductResponse> proResp = new ArrayList<>();
		for (Products prod : products) {
			ProductResponse productResponse = (ProductResponse) mapResponse(prod, new ProductResponse());
			String[] relatedImgArray = productResponse.getRelated_img().split(",");
			List<String> relatedImgList = new ArrayList<>();
			for (String relatedImg : relatedImgArray) {
				relatedImgList.add(context + "/" + relatedImg);
			}
			productResponse.setRelatedImgList(relatedImgList);
			proResp.add(productResponse);
		}
		return proResp;
	}

	/**
	 * Mapping Utils
	 * 
	 * @param src
	 * @param destination
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T> T mapResponse(T src, T destination) {
		logger.debug("Mapping Products Src: " + src.toString());
		if (src == null || destination == null)
			throw new NullPointerException(
					"Both the params must not be Null .. [src]=[" + src + "] ,Destination: " + destination);
		mapperFactory.classMap(src.getClass(), destination.getClass());
		MapperFacade mapper = mapperFactory.getMapperFacade();
		destination = (T) mapper.map(src, destination.getClass());
		logger.debug("Mapping Products Destination: " + destination.toString());
		return destination;
	}

	@Override
	public EntitySaveResponse saveProductResponse(ProductSavRq product) {
		logger.info("Save Product Service Called");
		Products productEntity = (Products) mapResponse(product, new Products());
		productEntity = productsRepository.save(productEntity);
		logger.info("Save Product Service End :");

		if (productEntity.getId() != null) {
			Products prod = productEntity;
			productEntity.getRates().forEach((Consumer<? super Rate>) (data) -> {
				data.setProduct(prod);
				rateRepository.save(data);
				logger.info("Rate Save Status: " + data.getId() != null ? true : false);
			});
			return new EntitySaveResponse(true, productEntity.getId());
		}
		return new EntitySaveResponse(false, productEntity.getId());
	}

	@Override
	public List<ProductDetails> getAllProductDetails() {
		logger.info("Getting all products_details service start");
		List<ProductDetailsEntity> products = productsDetailsRepository.findAll();
		List<ProductDetails> proResp = new ArrayList<>();
		for (ProductDetailsEntity prod : products) {
			ProductDetails productResponse = (ProductDetails) mapResponse(prod, new ProductDetails());
			proResp.add(productResponse);
		}
		return proResp;
	}

}
