package com.dream.service;

import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.dream.pojo.ContactSupport;
import com.dream.pojo.PropertiesFile;

@Service
public class ContactSupportServiceImpl implements ContactSupportService {

	@Autowired
	private Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private PropertiesFile prop;

	@Override
	@Async
	public void contactSupport(ContactSupport contactSupport) {
		logger.info("Contact Support Called :" + contactSupport.toString());
		sendContactSupportMail(contactSupport);
		sendContactSupportRecievdMail(contactSupport);

	}

	@Async
	public void sendContactSupportMail(ContactSupport contactSupport) {
		try {
//			ORDER_ID,DELIVERY_DATE
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(prop.getSelfEmailId());
			helper.setTo(prop.getSelfEmailId());
			helper.setSubject("Contact Support Form Called For " + contactSupport.getEmail());
			helper.setText(contactSupport.getName() + "<br/><br/>" + contactSupport.getDesc(), true);
			emailSender.send(message);
			logger.info("Mail Sent");
		} catch (Exception e) {
			logger.error("Exception while confirmation mail: " + e.getMessage(), e);
		}
	}

	@Async
	public void sendContactSupportRecievdMail(ContactSupport contactSupport) {
		try {
//			ORDER_ID,DELIVERY_DATE
			logger.info("Sending Contact Support recieved Mail");
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(prop.getAlert_email());
			helper.setTo(contactSupport.getEmail());
			helper.setSubject("Request Recieved");
			String msg = prop.getSupportConfirmationTemplate().replace("{{FIRSTNAME}}", contactSupport.getName());
			helper.setText(msg, true);
			emailSender.send(message);
			logger.info("Mail Sent");
		} catch (Exception e) {
			logger.error("Exception while confirmation mail: " + e.getMessage(), e);
		}
	}

}
