package com.dream.service;

import java.util.List;

import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.EntityUpdateResponse;
import com.dream.pojo.OrderRequestBean;
import com.dream.pojo.UserOrderDetailsResponse;

public interface OrderDetailsService {
	
	public EntitySaveResponse saveOrder(OrderRequestBean orderRequestBean);
	
	public List<UserOrderDetailsResponse> getOrderDetailsByUserId(Integer id);
	
	public List<UserOrderDetailsResponse> getAllOrderDetails();
	
	public List<UserOrderDetailsResponse> getOrdersByRange(Long startTimeInMills , Long endTime);
	
	public List<UserOrderDetailsResponse> getOrdersByRangeAndUserId(Long startTimeInMills , Long endTime , Integer userId);
	
	public EntityUpdateResponse updateOrderStatus(Integer orderId , String Status);
}
