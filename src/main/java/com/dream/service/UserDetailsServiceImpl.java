package com.dream.service;

import java.util.Calendar;
import java.util.Random;

import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.dream.entity.User;
import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.OtpHolder;
import com.dream.pojo.OtpMapper;
import com.dream.pojo.PropertiesFile;
import com.dream.pojo.UserDetailsResponse;
import com.dream.repo.UserRepository;
import com.dream.security.UserPrincipals;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, CustomUserDetailsService {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private PropertiesFile prop;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private MapperService mapperService;

	@Autowired
	private JavaMailSender emailSender;

	private static Logger logger = LogManager.getLogger();

	

	@Override
	public UserPrincipals loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("LoadByUsername Called..");
		User user = userRepo.findByUsername(username);
		StringBuffer msg = new StringBuffer();
		if (user == null) {
			msg.append("Username: ");
			msg.append(username);
			msg.append("Not found..");
			throw new UsernameNotFoundException(msg.toString());
		}
		logger.info("LoadByUsername End..");
		UserDetailsResponse userDetailsRsp = new UserDetailsResponse();
		userDetailsRsp = (UserDetailsResponse) mapperService.mapResponse(user, userDetailsRsp);
		return new UserPrincipals(user, userDetailsRsp);

	}

	@Override
	public EntitySaveResponse saveUser(SaveUserReq saveUser) {
		logger.info("Saving User..");

		User userEntity = new User();
		userEntity = (User) mapperService.mapResponse(saveUser, userEntity);
		userEntity.setUsername(saveUser.getEmail());
		userEntity.setPassword(passwordEncoder.encode(saveUser.getPassword()));
		userEntity = userRepo.save(userEntity);
		return (userEntity.getId() == null) ? new EntitySaveResponse(false, null)
				: new EntitySaveResponse(true, userEntity.getId());
	}

	@Override
	public Boolean forgetPassword(String username) {
		User userEntity = userRepo.findByUsername(username);
		if (userEntity == null)
			return false;
		try {

			MimeMessage message = emailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(prop.getAlert_email());
			helper.setTo(username);
			helper.setSubject("Forgot Password OTP");
			String otp = generateOTP(4);
			String msgString = prop.getOtpSendingMessage().replace("{{OTP}}", otp);
			helper.setText(msgString, true);
			OtpMapper.getInstance().getOtpMapper().put(username, new OtpHolder(otp));
			emailSender.send(message);
		} catch (Exception e) {
			logger.error("Exception while sending mail: " + e.getMessage(), e);
			return false;
		}
		return true;
	}

	private String generateOTP(int length) {
		String numbers = "1234567890";
		Random random = new Random();
		char[] otp = new char[length];

		for (int i = 0; i < length; i++) {
			otp[i] = numbers.charAt(random.nextInt(numbers.length()));
		}
		return new String(otp);
	}

	@Override
	public Boolean validateOtp(String username, String otp) {
		OtpHolder otpholder = OtpMapper.getInstance().getOtpMapper().get(username);
		if (otpholder != null && otpholder.getOtp().equals(otp)
				&& otpholder.getExpireTime() > Calendar.getInstance().getTimeInMillis()) {
			return true;
		}
		return false;
	}

	@Override
	public Boolean updatePassword(String username, String password) {
		User user = userRepo.findByUsername(username);
		String encodedPass = passwordEncoder.encode(password);
		user.setPassword(encodedPass);
		user = userRepo.save(user);
		if (user.getPassword().equals(encodedPass))
			return true;
		return false;
	}

	@Override
	public Boolean userExist(String username) {
		User user = userRepo.findByUsername(username);
		return user != null;
	}

}
