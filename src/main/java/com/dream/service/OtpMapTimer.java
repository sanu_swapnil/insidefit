package com.dream.service;

import java.util.Calendar;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dream.pojo.OtpHolder;
import com.dream.pojo.OtpMapper;

@Component
public class OtpMapTimer extends TimerTask {

	

	@Autowired
	private Logger logger = LogManager.getFormatterLogger();
	
	@Override
	public void run() {
		System.out.println("Started Doing My Work");
		System.out.println(OtpMapper.getInstance());
		if(OtpMapper.getInstance()!= null) {
			Map<String, OtpHolder> map = OtpMapper.getInstance().getOtpMapper();
			System.out.println(OtpMapper.getInstance().getOtpMapper());
			System.out.println(map);
			for (Map.Entry<String, OtpHolder> entryMap : map.entrySet()) {
				if (entryMap.getValue().getExpireTime() < Calendar.getInstance().getTimeInMillis()) {
					logger.info("Otp Expired Removing Key: " + entryMap.getKey());
					OtpMapper.getInstance().getOtpMapper().remove(entryMap.getKey());
				} else {
					logger.info("Not removing key");
				}
			}
		}
		

	}

}
