package com.dream.service;

import java.util.List;

import com.dream.pojo.AddToCartReq;
import com.dream.pojo.CartDetails;
import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.EntityUpdateResponse;

public interface CartService {

	public EntitySaveResponse addToCart(AddToCartReq addTocartReq);
	
	public List<CartDetails> getCartDetailsByUser(Integer userId);
	
	public EntityUpdateResponse removeFromCart(Integer id);
	
}
