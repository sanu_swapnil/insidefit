package com.dream.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dream.entity.CartEntity;
import com.dream.pojo.AddToCartReq;
import com.dream.pojo.CartDetails;
import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.EntityUpdateResponse;
import com.dream.repo.CartRepository;

@Service
public class CartServiceImpl implements CartService {

	@Autowired
	private MapperService mapperService;

	private static Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private CartRepository cartRepository;

	@Override
	public EntitySaveResponse addToCart(AddToCartReq addTocartReq) {
		logger.info("Adding to Cart..");
		CartEntity cartEntity = (CartEntity) mapperService.mapResponse(addTocartReq, new CartEntity());
		cartEntity = cartRepository.save(cartEntity);
		return (cartEntity.getId() == null) ? new EntitySaveResponse(false, null)
				: new EntitySaveResponse(true, cartEntity.getId());
	}

	@Override
	public List<CartDetails> getCartDetailsByUser(Integer userId) {
		logger.info("Getting Cart_Details for user: " + userId);
		List<CartEntity> cartEntityList = cartRepository.findByUserId(userId);
		List<CartDetails> cartDetailList = new ArrayList<>();
		for (CartEntity cartEntity : cartEntityList) {
			CartDetails cartDetail = new CartDetails();
			cartDetail = (CartDetails) mapperService.mapResponse(cartEntity, cartDetail);
			cartDetailList.add(cartDetail);
		}
		return cartDetailList;
	}

	@Override
	@org.springframework.transaction.annotation.Transactional
	public EntityUpdateResponse removeFromCart(Integer id) {
		logger.info("Removing Cart with id: " + id);
		cartRepository.deleteById(id);
		return new EntityUpdateResponse(true, 1 + " records Deleted");

	}
}
