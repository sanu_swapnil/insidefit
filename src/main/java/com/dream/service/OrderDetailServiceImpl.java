package com.dream.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.dream.entity.OrderDetailsEntity;
import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.EntityUpdateResponse;
import com.dream.pojo.OrderRequestBean;
import com.dream.pojo.PropertiesFile;
import com.dream.pojo.Status;
import com.dream.pojo.UserOrderDetailsResponse;
import com.dream.repo.OrderDetailsRepository;
import com.dream.repo.ProductsRepository;

@Service
public class OrderDetailServiceImpl implements OrderDetailsService {

	@Autowired
	private MapperService mapperService;

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private PropertiesFile prop;

	private static Logger logger = LogManager.getFormatterLogger();

	@Autowired
	private ProductsRepository productsRepository;

	@Autowired
	private OrderDetailsRepository orderDetailsRepository;

	@Override
	public EntitySaveResponse saveOrder(OrderRequestBean orderRequestBean) {
		logger.info("Saving Order..");
		OrderDetailsEntity orderEntity = (OrderDetailsEntity) mapperService.mapResponse(orderRequestBean,
				new OrderDetailsEntity());
		List<Integer> prodList = new ArrayList<Integer>();
		for (Integer prodId : orderRequestBean.getProductIds()) {
			prodList.add(prodId);
		}
		orderEntity.setProductId(prodList);
		orderEntity.setOrderId(RandomStringUtils.randomAlphanumeric(10));
		orderEntity.setCurrentOrderStatus(Status.ORDERED.getName());
		orderEntity.setStartTime(Calendar.getInstance().getTimeInMillis());
		orderEntity.setExpectedDeliveryTimeInMills((long) (orderEntity.getStartTime() + 1.296e+9));
		orderEntity = orderDetailsRepository.save(orderEntity);
		if((orderEntity.getId() == null))
			new EntitySaveResponse(false, null);
		sendOrderConfirmationEmail(orderEntity.getUsername(), orderEntity.getOrderId(),formatDate(orderEntity.getExpectedDeliveryTimeInMills()));
		sendOrderRecievedEmail(orderEntity.getUsername(), orderEntity.getOrderId(),formatDate(orderEntity.getExpectedDeliveryTimeInMills()));
		logger.info("Triggered Mail Sending");
		return new EntitySaveResponse(true, orderEntity.getId());
	}

	@Async
	public void sendOrderConfirmationEmail(String username, String orderId, String expectedDelivery) {
		try {
//			ORDER_ID,DELIVERY_DATE
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(prop.getAlert_email());
			helper.setTo(username);
			helper.setSubject("Order Confirmation");
			String msgString = prop.getMailConfirmationTemplate().replace("{{ORDER_ID}}", orderId)
					.replace("{{DELIVERY_DATE}}", expectedDelivery);
			helper.setText(msgString, true);
			emailSender.send(message);
			logger.info("Mail Sent");
		} catch (Exception e) {
			logger.error("Exception while confirmation mail: " + e.getMessage(), e);
		}
	}

	@Async
	public void sendOrderRecievedEmail(String username, String orderId, String expectedDelivery) {
		try {
//			ORDER_ID,DELIVERY_DATE
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(prop.getAlert_email());
			helper.setTo(prop.getSelfEmailId());
			helper.setSubject("Order Recieved");
			String msgString = prop.getMailConfirmationTemplate().replace("{{ORDER_ID}}", orderId)
					.replace("{{DELIVERY_DATE}}", expectedDelivery);
			helper.setText(msgString, true);
			emailSender.send(message);
			logger.info("Mail Sent");
		} catch (Exception e) {
			logger.error("Exception while confirmation mail: " + e.getMessage(), e);
		}
	}

	private String formatDate(long dateInMills) {
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(new Date(dateInMills));
	}

	@Override
	public List<UserOrderDetailsResponse> getOrderDetailsByUserId(Integer id) {
		logger.info("getOrderDetailsByUserId called..");
		List<OrderDetailsEntity> orderDetailEntityList = orderDetailsRepository.findOrderByUserId(id);
		List<UserOrderDetailsResponse> orderDetailResponseList = mapOrderDetailResults(orderDetailEntityList);
		logger.info("getOrderDetailsByUserId end..");
		return orderDetailResponseList;
	}

	@Override
	public List<UserOrderDetailsResponse> getAllOrderDetails() {
		logger.info("getAllOrderDetails called..");
		List<OrderDetailsEntity> orderDetailEntityList = orderDetailsRepository.findAll();
		List<UserOrderDetailsResponse> orderDetailResponseList = mapOrderDetailResults(orderDetailEntityList);
		logger.info("getAllOrderDetails end..");
		return orderDetailResponseList;
	}

	@Override
	public List<UserOrderDetailsResponse> getOrdersByRange(Long startTimeInMills, Long endTime) {
		logger.info("getOrdersByRange called..");
		List<OrderDetailsEntity> orderDetailEntityList = orderDetailsRepository.findOrderByRange(startTimeInMills,
				endTime);
		List<UserOrderDetailsResponse> orderDetailResponseList = mapOrderDetailResults(orderDetailEntityList);
		logger.info("getOrdersByRange end..");
		return orderDetailResponseList;
	}

	@Override
	public List<UserOrderDetailsResponse> getOrdersByRangeAndUserId(Long startTimeInMills, Long endTime,
			Integer userId) {
		logger.info("getOrdersByRangeAndUserId called..");
		List<OrderDetailsEntity> orderDetailEntityList = orderDetailsRepository.findOrderByRange(startTimeInMills,
				endTime);
		List<UserOrderDetailsResponse> orderDetailResponseList = mapOrderDetailResults(orderDetailEntityList);
		logger.info("getOrdersByRangeAndUserId end..");
		return orderDetailResponseList;
	}

	private List<UserOrderDetailsResponse> mapOrderDetailResults(List<OrderDetailsEntity> orderDetailEntityList) {
		List<UserOrderDetailsResponse> orderDetailResponseList = new ArrayList<>();
		if (orderDetailEntityList != null) {
			for (OrderDetailsEntity orderDetailEntity : orderDetailEntityList) {
				UserOrderDetailsResponse userOrderDetailsResponse = (UserOrderDetailsResponse) mapperService
						.mapResponse(orderDetailEntity, new UserOrderDetailsResponse());
				List<Integer> productIds = new ArrayList<Integer>();
				for (Integer productId : orderDetailEntity.getProductId()) {
					productIds.add(productId);
				}
				userOrderDetailsResponse.setProductIds(productIds);
				orderDetailResponseList.add(userOrderDetailsResponse);
			}
		}
		return orderDetailResponseList;
	}

	@Override
	public EntityUpdateResponse updateOrderStatus(Integer orderId, String status) {
		Optional<OrderDetailsEntity> orderDetailsEntity = orderDetailsRepository.findById(orderId);
		if (orderDetailsEntity.isPresent()) {
			if (status.equals(Status.COMPLETED.getName()))
				orderDetailsEntity.get().setEndTime(Calendar.getInstance().getTimeInMillis());
			orderDetailsEntity.get().setCurrentOrderStatus(status);
			orderDetailsRepository.save(orderDetailsEntity.get());
			if (orderDetailsEntity.get().getCurrentOrderStatus().equals(status))
				return new EntityUpdateResponse(true, "Updation Successfull");
			return new EntityUpdateResponse(false, "Error Occured While Updation");
		} else {
			return new EntityUpdateResponse(false, "Order_Id Not Found");
		}
	}

}
