package com.dream.service;

import java.util.List;

import com.dream.controller.ProductSavRq;
import com.dream.pojo.EntitySaveResponse;
import com.dream.pojo.ProductDetails;
import com.dream.pojo.ProductResponse;

public interface ProductService {
	
	public List<ProductResponse> getAllProducts(String context);
	
	public List<ProductDetails> getAllProductDetails();

	public EntitySaveResponse saveProductResponse(ProductSavRq product);
	
	
}
