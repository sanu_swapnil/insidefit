package com.dream.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "order_details")
public class OrderDetailsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Long startTime;

	private String orderId;
	
	private Long endTime;

	private Long expectedDeliveryTimeInMills;

	private String feedback;

	private String currentOrderStatus;

	@Column(columnDefinition = "TEXT")
	private String deliveryAddress;

	private Integer userId;

	@ElementCollection(targetClass = Integer.class, fetch = FetchType.EAGER)
	private List<Integer> productIds;

	private String trackingId;

	private Float orderAmount;

	private String paymentId;

	@Lob
	private String productDetails;
	
	private String username;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the startTime
	 */
	public Long getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public Long getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the expectedDeliveryTimeInMills
	 */
	public Long getExpectedDeliveryTimeInMills() {
		return expectedDeliveryTimeInMills;
	}

	/**
	 * @param expectedDeliveryTimeInMills the expectedDeliveryTimeInMills to set
	 */
	public void setExpectedDeliveryTimeInMills(Long expectedDeliveryTimeInMills) {
		this.expectedDeliveryTimeInMills = expectedDeliveryTimeInMills;
	}

	/**
	 * @return the feedback
	 */
	public String getFeedback() {
		return feedback;
	}

	/**
	 * @param feedback the feedback to set
	 */
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	/**
	 * @return the currentOrderStatus
	 */
	public String getCurrentOrderStatus() {
		return currentOrderStatus;
	}

	/**
	 * @param currentOrderStatus the currentOrderStatus to set
	 */
	public void setCurrentOrderStatus(String currentOrderStatus) {
		this.currentOrderStatus = currentOrderStatus;
	}

	/**
	 * @return the deliveryAddress
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the trackingId
	 */
	public String getTrackingId() {
		return trackingId;
	}

	/**
	 * @param trackingId the trackingId to set
	 */
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	/**
	 * @return the orderAmount
	 */
	public Float getOrderAmount() {
		return orderAmount;
	}

	/**
	 * @param orderAmount the orderAmount to set
	 */
	public void setOrderAmount(Float orderAmount) {
		this.orderAmount = orderAmount;
	}

	/**
	 * @return the paymentId
	 */
	public String getPaymentId() {
		return paymentId;
	}

	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * @return the productId
	 */
	public List<Integer> getProductId() {
		return productIds;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(List<Integer> productId) {
		this.productIds = productId;
	}

	/**
	 * @return the productDetails
	 */
	public String getProductDetails() {
		return productDetails;
	}

	/**
	 * @param productDetails the productDetails to set
	 */
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "OrderDetailsEntity [id=" + id + ", startTime=" + startTime + ", orderId=" + orderId + ", endTime="
				+ endTime + ", expectedDeliveryTimeInMills=" + expectedDeliveryTimeInMills + ", feedback=" + feedback
				+ ", currentOrderStatus=" + currentOrderStatus + ", deliveryAddress=" + deliveryAddress + ", userId="
				+ userId + ", productIds=" + productIds + ", trackingId=" + trackingId + ", orderAmount=" + orderAmount
				+ ", paymentId=" + paymentId + ", productDetails=" + productDetails + ", username=" + username + "]";
	}


}
