package com.dream.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product_details")
public class ProductDetailsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String productName;

	private Integer rateCount;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
	private List<VideoEntity> videos;

	private String productImages;

	private String productTitle;

	private String productHeading;

	private Float price;
	
	private String currency;
	
	@Lob
	@Column(length = 100000)
	private String description;

	@OneToMany(mappedBy = "product")
	private List<ReviewEntity> reviews;

	private String productSub;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the rateCount
	 */
	public Integer getRateCount() {
		return rateCount;
	}

	/**
	 * @param rateCount the rateCount to set
	 */
	public void setRateCount(Integer rateCount) {
		this.rateCount = rateCount;
	}

	/**
	 * @return the videos
	 */
	public List<VideoEntity> getVideos() {
		return videos;
	}

	/**
	 * @param videos the videos to set
	 */
	public void setVideos(List<VideoEntity> videos) {
		this.videos = videos;
	}

	/**
	 * @return the productImages
	 */
	public String getProductImages() {
		return productImages;
	}

	/**
	 * @param productImages the productImages to set
	 */
	public void setProductImages(String productImages) {
		this.productImages = productImages;
	}

	/**
	 * @return the productTitle
	 */
	public String getProductTitle() {
		return productTitle;
	}

	/**
	 * @param productTitle the productTitle to set
	 */
	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	/**
	 * @return the productHeading
	 */
	public String getProductHeading() {
		return productHeading;
	}

	/**
	 * @param productHeading the productHeading to set
	 */
	public void setProductHeading(String productHeading) {
		this.productHeading = productHeading;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the reviews
	 */
	public List<ReviewEntity> getReviews() {
		return reviews;
	}

	/**
	 * @param reviews the reviews to set
	 */
	public void setReviews(List<ReviewEntity> reviews) {
		this.reviews = reviews;
	}

	
	/**
	 * @return the price
	 */
	public Float getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Float price) {
		this.price = price;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the productSub
	 */
	public String getProductSub() {
		return productSub;
	}

	/**
	 * @param productSub the productSub to set
	 */
	public void setProductSub(String productSub) {
		this.productSub = productSub;
	}

	@Override
	public String toString() {
		return "ProductDetailsEntity [id=" + id + ", productName=" + productName + ", rateCount=" + rateCount
				+ ", videos=" + videos + ", productImages=" + productImages + ", productTitle=" + productTitle
				+ ", productHeading=" + productHeading + ", price=" + price + ", currency=" + currency
				+ ", description=" + description + ", reviews=" + reviews + ", productSub=" + productSub + "]";
	}

}
