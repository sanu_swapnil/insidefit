package com.dream.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_rates")
public class Rate {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String dimension;

	private String currency;

	private Float actualPrice;

	private Float afterDiscountPrice;

	private Float discountInPercentage;

	@ManyToOne()
	private Products product;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the dimension
	 */
	public String getDimension() {
		return dimension;
	}

	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the actualPrice
	 */
	public Float getActualPrice() {
		return actualPrice;
	}

	/**
	 * @param actualPrice the actualPrice to set
	 */
	public void setActualPrice(Float actualPrice) {
		this.actualPrice = actualPrice;
	}

	/**
	 * @return the afterDiscountPrice
	 */
	public Float getAfterDiscountPrice() {
		return afterDiscountPrice;
	}

	/**
	 * @param afterDiscountPrice the afterDiscountPrice to set
	 */
	public void setAfterDiscountPrice(Float afterDiscountPrice) {
		this.afterDiscountPrice = afterDiscountPrice;
	}

	/**
	 * @return the discountInPercentage
	 */
	public Float getDiscountInPercentage() {
		return discountInPercentage;
	}

	/**
	 * @param discountInPercentage the discountInPercentage to set
	 */
	public void setDiscountInPercentage(Float discountInPercentage) {
		this.discountInPercentage = discountInPercentage;
	}

	/**
	 * @return the product
	 */
	public Products getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Products product) {
		this.product = product;
	}


}
