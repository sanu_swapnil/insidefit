public class Test {
	public int a = 0;

	public void add() {
		a += 2;
		System.out.println(a);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		CommonObj obj = new CommonObj();
//		Producer p = new Producer(obj);
//		Consumer c = new Consumer(obj);
//		p.start();
//		c.start();
//		Integer i = 10;

//		String s1 = "test";
//		String s2 = "test";
//		System.out.println(s1 == s2);
//		System.out.println(s1.equals(s2));

//	    String generatedString = RandomStringUtils.randomAlphanumeric(10);
//	 
//	    System.out.println(generatedString);

//		String[] strings = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
//
//		System.out.println("-------\nRunning sequential\n-------");
//		run(Arrays.stream(strings).sequential().filter((t) -> {
//			if (new Integer(t) % 2 == 0) {
//				return true;
//			}
//			return false;
//		}));
//		System.out.println("-------\nRunning parallel\n-------");
//		run(Arrays.stream(strings).parallel().filter((t) -> {
//			if (new Integer(t) % 2 != 0) {
//				return true;
//			}
//			return false;
//		}));

//		Test a = new B();
//		a.add();
//		System.out.println(a.a);
//		Map<String, String> map = new HashMap<>();
//		map.put(null, null);
//		System.out.println(map.size());
//		map.put(null, "abc");
//		System.out.println(map.size());
//		System.out.println(map.get(null));
//		System.out.println(map.get(0));

//		Testing tes = new Testing();
//		Thread t = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//
//				tes.m1();
//				
//			}
//		});
//
//		Thread t2 = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//
//				tes.m2();
//				
//			}
//		});
//		t.start();
//		t2.start();
		System.out.println(new Main1().print());
		System.out.println(new Submain().print());
	}

	
	
	public int getA() {
		return a;
	}
//	public static void run(Stream<String> stream) {
//
//		stream.forEach(s -> {
//			System.out.println(LocalTime.now() + " - value: " + s + " - thread: " + Thread.currentThread().getName());
//			try {
//				Thread.sleep(200);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		});
//	}
}

class Main1{
	public Main2 print() {
		return new Main2();
	}
}

class Main2{
	
}
class Main3 extends Main2{
	
}
class Submain extends Main1{
	@Override
	public Main3 print() {
		return new Main3();
	}
}
class B extends Test {
	public int a = 2;

	public void add() {
		a += 6;
		System.out.println(a);
	}

	public int getA() {
		return a;
	}

}

class Testing {
	synchronized void m1() {
		System.out.println("In m1 A");
		try {
			Thread.sleep(10000);
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("In m1 A complete");
	}

	 void m2() {
		System.out.println("In m2 A");
	}
}